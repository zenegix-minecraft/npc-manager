package ru.zenegix.npcmanager.protocollib.adapter;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.plugin.Plugin;
import ru.zenegix.npcmanager.Npc;
import ru.zenegix.npcmanager.NpcManager;
import ru.zenegix.npcmanager.bukkit.PlayerMapper;
import ru.zenegix.npcmanager.protocollib.packet.WrapperPlayClientUseEntity;

public class EntityListener<P> extends PacketAdapter {

    private final NpcManager<P> npcManager;

    private final PlayerMapper<P> playerMapper;

    public EntityListener(Plugin plugin, NpcManager<P> npcManager, PlayerMapper<P> playerMapper) {
        super(plugin, PacketType.Play.Client.USE_ENTITY);
        this.npcManager = npcManager;
        this.playerMapper = playerMapper;
    }

    @Override
    public void onPacketReceiving(final PacketEvent event) {
        WrapperPlayClientUseEntity packet = new WrapperPlayClientUseEntity(event.getPacket());
        int id = packet.getTargetID();

        Npc<P> npc = this.npcManager.getNpc(id);

        if (npc == null) {
            return;
        }

        switch (packet.getType()) {
            case INTERACT: {
                npc.handleClick(this.playerMapper.convert(event.getPlayer()), Npc.ClickType.RIGHT);

                break;
            }
            case ATTACK: {
                npc.handleClick(this.playerMapper.convert(event.getPlayer()), Npc.ClickType.LEFT);

                break;
            }
        }
    }

}
