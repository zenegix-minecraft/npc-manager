package ru.zenegix.npcmanager.protocollib;

import com.comphenix.protocol.ProtocolLibrary;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import ru.zenegix.npcmanager.bukkit.BukkitInternal;
import ru.zenegix.npcmanager.bukkit.PlayerMapper;
import ru.zenegix.npcmanager.impl.SimpleNpcManager;
import ru.zenegix.npcmanager.player.PlayerService;
import ru.zenegix.npcmanager.protocollib.adapter.EntityListener;
import ru.zenegix.npcmanager.task.UpdateNpcViewersTask;

public class ProtocolNpcManager<P> extends SimpleNpcManager<P> {

    public ProtocolNpcManager(Plugin plugin, PlayerMapper<P> playerMapper, PlayerService<P> playerService) {
        super(new BukkitInternal(), new ProtocolLibPacketFactory<>(playerMapper));

        ProtocolLibrary.getProtocolManager().addPacketListener(
                new EntityListener<>(plugin, this, playerMapper)
        );
        Bukkit.getScheduler().runTaskTimer(
                plugin,
                new UpdateNpcViewersTask<>(this, playerService),
                20,
                20
        );
    }

}
