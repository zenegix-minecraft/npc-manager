package ru.zenegix.npcmanager.protocollib;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import ru.zenegix.npcmanager.bukkit.BukkitPlayerServiceAndMapper;

public class SimpleProtocolNpcManager extends ProtocolNpcManager<Player> {

    public SimpleProtocolNpcManager(Plugin plugin) {
        super(plugin, BukkitPlayerServiceAndMapper.INSTANCE, BukkitPlayerServiceAndMapper.INSTANCE);
    }

}
