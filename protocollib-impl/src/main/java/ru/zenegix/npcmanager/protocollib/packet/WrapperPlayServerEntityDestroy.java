package ru.zenegix.npcmanager.protocollib.packet;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import ru.zenegix.npcmanager.bukkit.PlayerMapper;
import ru.zenegix.npcmanager.packet.NpcDespawnPacket;
import ru.zenegix.npcmanager.protocollib.AbstractPacket;

public class WrapperPlayServerEntityDestroy<P> extends AbstractPacket implements NpcDespawnPacket<P> {

    public static final PacketType TYPE = PacketType.Play.Server.ENTITY_DESTROY;

    private final PlayerMapper<P> playerMapper;

    public WrapperPlayServerEntityDestroy(int id, PlayerMapper<P> playerMapper) {
        super(new PacketContainer(TYPE), TYPE);
        this.playerMapper = playerMapper;

        handle.getModifier().writeDefaults();

        this.setEntityIds(new int[] {id});
    }

    /**
     * Retrieve Count.
     * <p>
     * Notes: length of following array
     *
     * @return The current Count
     */
    public int getCount() {
        return handle.getIntegerArrays().read(0).length;
    }

    /**
     * Retrieve Entity IDs.
     * <p>
     * Notes: the list of entities of destroy
     *
     * @return The current Entity IDs
     */
    public int[] getEntityIDs() {
        return handle.getIntegerArrays().read(0);
    }

    /**
     * Set Entity IDs.
     *
     * @param value - new value.
     */
    public void setEntityIds(int[] value) {
        handle.getIntegerArrays().write(0, value);
    }

    @Override
    public void send(P player) {
        this.sendPacket(this.playerMapper.convert(player));
    }
}
