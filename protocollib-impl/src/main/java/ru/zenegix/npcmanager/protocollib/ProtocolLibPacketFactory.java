package ru.zenegix.npcmanager.protocollib;

import ru.zenegix.npcmanager.bukkit.PlayerMapper;
import ru.zenegix.npcmanager.packet.NpcDespawnPacket;
import ru.zenegix.npcmanager.packet.NpcSpawnPacket;
import ru.zenegix.npcmanager.packet.PacketFactory;
import ru.zenegix.npcmanager.protocollib.packet.WrapperPlayServerEntityDestroy;
import ru.zenegix.npcmanager.protocollib.packet.WrapperPlayServerSpawnEntityLiving;

import java.util.UUID;

public class ProtocolLibPacketFactory<P> implements PacketFactory<P> {

    private final PlayerMapper<P> playerMapper;

    public ProtocolLibPacketFactory(PlayerMapper<P> playerMapper) {
        this.playerMapper = playerMapper;
    }

    @Override
    public NpcSpawnPacket<P> createSpawnPacket(int npcId, UUID uuid, int type) {
        return new WrapperPlayServerSpawnEntityLiving<>(npcId, uuid, type, this.playerMapper);
    }

    @Override
    public NpcDespawnPacket<P> createDespawnPacket(int npcId) {
        return new WrapperPlayServerEntityDestroy<>(npcId, this.playerMapper);
    }

}
