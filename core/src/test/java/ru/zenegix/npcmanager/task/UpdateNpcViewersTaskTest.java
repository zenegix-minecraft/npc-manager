package ru.zenegix.npcmanager.task;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.zenegix.npcmanager.Npc;
import ru.zenegix.npcmanager.NpcManager;
import ru.zenegix.npcmanager.TestPlayer;
import ru.zenegix.npcmanager.player.PlayerService;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

public class UpdateNpcViewersTaskTest {

    TestPlayer player;

    Collection<TestPlayer> players;

    PlayerService<TestPlayer> playerService;

    Npc<TestPlayer> npc;

    NpcManager<TestPlayer> npcManager;

    @Before
    public void before() {
        this.player = new TestPlayer();
        this.players = Collections.singletonList(this.player);
        this.playerService = mock(PlayerService.class);
        when(this.playerService.getAll()).thenReturn(this.players);

        this.npc = mock(Npc.class);
        this.npcManager = mock(NpcManager.class);

        when(this.npcManager.getAllNpc()).thenReturn(Collections.singletonList(this.npc));
    }

    @Test
    public void shouldSendSpawnPacket() {
        work(npc -> npc.spawn(any()), 100D, true);
    }

    @Test
    public void shouldNotSendSpawnPacket() {
        work(npc -> npc.spawn(any()), (double) (300 * 300), false);
    }

    @Test
    public void shouldSendDespawnPacket() {
        work(npc -> npc.despawn(any()), (double) (300 * 300), true);
    }

    @Test
    public void shouldNotSendDespawnPacket() {
        work(npc -> npc.despawn(any()), 100D, false);
    }

    private void work(Consumer<Npc<?>> consumer, Double distance, boolean expect) {
        AtomicBoolean result = new AtomicBoolean(false);
        when(this.playerService.distance(any(), any())).thenReturn(distance);

        consumer.accept(doAnswer(t -> {
            result.set(true);

            return Void.TYPE;
        }).when(this.npc));

        new UpdateNpcViewersTask<>(this.npcManager, this.playerService).run();

        Assert.assertEquals(result.get(), expect);
    }

}
