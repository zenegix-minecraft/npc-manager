package ru.zenegix.npcmanager.task;

import ru.zenegix.npcmanager.Npc;
import ru.zenegix.npcmanager.NpcManager;
import ru.zenegix.npcmanager.data.NpcPosition;
import ru.zenegix.npcmanager.player.PlayerService;

import java.util.Collection;

public class UpdateNpcViewersTask<P> implements Runnable {

    private final NpcManager<P> npcManager;

    private final PlayerService<P> playerService;

    private final int VISIBLE_DISTANCE = 256 * 256; // in square

    public UpdateNpcViewersTask(NpcManager<P> npcManager, PlayerService<P> playerService) {
        this.npcManager = npcManager;
        this.playerService = playerService;
    }

    @Override
    public void run() {
        Collection<Npc<P>> npcs = this.npcManager.getAllNpc();
        Collection<P> players = this.playerService.getAll();

        if (npcs.isEmpty() || players.isEmpty()) {
            return;
        }

        for (Npc<P> npc : npcs) {
            NpcPosition npcPosition = npc.getPosition();

            for (P player : players) {
                double distance = this.playerService.distance(player, npcPosition);

                if (distance > VISIBLE_DISTANCE) {
                    npc.despawn(player);
                } else {
                    npc.spawn(player);
                }
            }
        }
    }

}
