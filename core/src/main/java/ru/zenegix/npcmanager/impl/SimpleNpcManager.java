package ru.zenegix.npcmanager.impl;

import ru.zenegix.npcmanager.Npc;
import ru.zenegix.npcmanager.NpcManager;
import ru.zenegix.npcmanager.data.NpcPosition;
import ru.zenegix.npcmanager.packet.PacketFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SimpleNpcManager<P> implements NpcManager<P> {

    private final Internal internal;

    private final PacketFactory<P> packetFactory;

    private final Set<Npc<P>> npcSet = new HashSet<>();

    public SimpleNpcManager(Internal internal, PacketFactory<P> packetFactory) {
        this.internal = internal;
        this.packetFactory = packetFactory;
    }

    @Override
    public Internal getInternal() {
        return this.internal;
    }

    @Override
    public PacketFactory<P> getPacketFactory() {
        return this.packetFactory;
    }

    @Override
    public Npc<P> createNpc(int type, NpcPosition position) {
        Npc<P> npc = new NpcImpl<>(this, type, position);

        this.npcSet.add(npc);

        return npc;
    }

    @Override
    public Npc<P> getNpc(int id) {
        return this.npcSet.stream()
                .filter(npc -> npc.getId() == id)
                .findFirst().orElse(null);
    }

    @Override
    public Collection<Npc<P>> getAllNpc() {
        return Collections.unmodifiableCollection(this.npcSet);
    }

}
