package ru.zenegix.npcmanager.impl;

import ru.zenegix.npcmanager.Npc;
import ru.zenegix.npcmanager.NpcManager;
import ru.zenegix.npcmanager.data.NpcPosition;
import ru.zenegix.npcmanager.packet.NpcDespawnPacket;
import ru.zenegix.npcmanager.packet.NpcSpawnPacket;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.function.BiConsumer;

public class NpcImpl<P> implements Npc<P> {

    private final int id;

    private final NpcSpawnPacket<P> spawnPacket;

    private final NpcDespawnPacket<P> despawnPacket;

    private final Set<P> players = new HashSet<>();

    private final NpcPosition position;

    private BiConsumer<P, ClickType> clickHandler = (__, ___) -> {};

    public NpcImpl(NpcManager<P> npcManager, int type, NpcPosition position) {
        this.id = npcManager.getInternal().getNextEntityId();
        this.position = position;
        UUID uuid = npcManager.getInternal().randomUUID();

        this.spawnPacket = npcManager.getPacketFactory().createSpawnPacket(this.id, uuid, type);
        this.spawnPacket.setXYZ(position.getX(), position.getY(), position.getZ());

        this.despawnPacket = npcManager.getPacketFactory().createDespawnPacket(this.id);
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void spawn(P player) {
        if (!this.isSpawned(player)) {
            this.spawnPacket.send(player);

            this.players.add(player);
        }
    }

    @Override
    public void despawn(P player) {
        if (this.isSpawned(player)) {
            this.despawnPacket.send(player);

            this.players.remove(player);
        }
    }

    @Override
    public boolean isSpawned(P player) {
        return this.players.contains(player);
    }

    @Override
    public NpcPosition getPosition() {
        return this.position;
    }

    @Override
    public void setClickHandler(BiConsumer<P, ClickType> clickHandler) {
        this.clickHandler = clickHandler;
    }

    @Override
    public void handleClick(P player, ClickType clickType) {
        this.clickHandler.accept(player, clickType);
    }

}
