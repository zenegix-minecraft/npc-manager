package ru.zenegix.npcmanager;

import ru.zenegix.npcmanager.data.NpcPosition;
import ru.zenegix.npcmanager.packet.PacketFactory;

import java.util.Collection;
import java.util.UUID;

public interface NpcManager<P> {

    Internal getInternal();

    PacketFactory<P> getPacketFactory();

    /**
     * Create the new npc instance with type
     *
     * @param type npc type
     * @param position position of npc
     * @return new npc
     */
    Npc<P> createNpc(int type, NpcPosition position);

    /**
     * Get the npc by given id
     *
     * If npc with given id not found, then return null
     *
     * @param id npc id
     * @return npc or null
     */
    Npc<P> getNpc(int id);

    /**
     * Get the all npc
     *
     * This method will return the unmodifiable collection
     *
     * @return collection of npc
     */
    Collection<Npc<P>> getAllNpc();

    interface Internal {

        UUID randomUUID();

        int getNextEntityId();

    }

}
