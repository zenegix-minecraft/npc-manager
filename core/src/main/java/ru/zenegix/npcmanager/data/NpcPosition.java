package ru.zenegix.npcmanager.data;

public class NpcPosition {

    private double x, y, z;

    private float yaw, pitch, headPitch;

    public NpcPosition(double x, double y, double z) {
        this(x, y, z, 0, 0);
    }

    public NpcPosition(double x, double y, double z, float yaw, float pitch) {
        this(x, y, z, yaw, pitch, pitch);
    }

    public NpcPosition(double x, double y, double z, float yaw, float pitch, float headPitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.headPitch = headPitch;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public void setHeadPitch(float headPitch) {
        this.headPitch = headPitch;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public float getYaw() {
        return this.yaw;
    }

    public float getPitch() {
        return this.pitch;
    }

    public float getHeadPitch() {
        return this.headPitch;
    }
}
