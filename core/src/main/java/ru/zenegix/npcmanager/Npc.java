package ru.zenegix.npcmanager;

import ru.zenegix.npcmanager.data.NpcPosition;

import java.util.function.BiConsumer;

public interface Npc<P> {

    /**
     * Get the id of this npc
     *
     * @return if of this npc
     */
    int getId();

    /**
     * Send the spawn packet to player
     *
     * @param player player
     */
    void spawn(P player);

    /**
     * Send the despawn packet to player
     *
     * @param player player
     */
    void despawn(P player);

    /**
     * Check if this npc is spawned for given player
     *
     * @param player player
     * @return {@code true} if npc is spawned, {@code false} otherwise
     */
    boolean isSpawned(P player);

    /**
     * Get the current position of the npc
     *
     * @return current position of the npc
     */
    NpcPosition getPosition();

    /**
     * Set the handler on any click
     *
     * Called when player issue any click of this npc
     *
     * @param clickHandler click handler
     */
    void setClickHandler(BiConsumer<P, ClickType> clickHandler);

    /**
     * Handle the click to npc by player
     *
     * @param player player who clicked to npc
     * @param clickType click type
     */
    void handleClick(P player, ClickType clickType);

    enum ClickType {
        RIGHT, LEFT;
    }

}
