package ru.zenegix.npcmanager.utils;

public class MathUtils {

    public static double distanceSquared(double x0, double y0, double z0, double x, double y, double z) {
        return square(x0 - x) +
                square(y0 - y)
                + square(z0 - z);
    }

    public static double square(double num) {
        return num * num;
    }

}
