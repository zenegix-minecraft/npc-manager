package ru.zenegix.npcmanager.player;

import ru.zenegix.npcmanager.data.NpcPosition;

import java.util.Collection;

public interface PlayerService<P> {

    /**
     * Get the distance from player to npc position
     *
     * @param player player
     * @param position npc position
     * @return squared distance
     */
    double distance(P player, NpcPosition position);

    /**
     * Get the all presents players
     *
     * @return collection of players
     */
    Collection<P> getAll();

}
