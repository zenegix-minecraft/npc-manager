package ru.zenegix.npcmanager.packet;

public interface NpcSpawnPacket<P> extends Packet<P> {

    void setX(double x);

    void setY(double y);

    void setZ(double z);

    void setYaw(float yaw);

    void setPitch(float pitch);

    void setHeadPitch(float headPitch);

    void setXYZ(double x, double y, double z);

}
