package ru.zenegix.npcmanager.packet;

public interface Packet<P> {

    void send(P player);

}
