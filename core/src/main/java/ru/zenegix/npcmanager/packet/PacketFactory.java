package ru.zenegix.npcmanager.packet;

import java.util.UUID;

public interface PacketFactory<P> {

    /**
     * Create the npc spawn packet
     *
     * @param npcId the npc id
     * @return spawn packet
     */
    NpcSpawnPacket<P> createSpawnPacket(int npcId, UUID uuid, int type);

    /**
     * Create the nps despawn packet
     *
     * @param npcId the npc id
     * @return despawn packet
     */
    NpcDespawnPacket<P> createDespawnPacket(int npcId);

}
