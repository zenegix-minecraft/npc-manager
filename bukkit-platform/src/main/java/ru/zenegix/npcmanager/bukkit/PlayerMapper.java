package ru.zenegix.npcmanager.bukkit;

import org.bukkit.entity.Player;

public interface PlayerMapper<P> {

    P convert(Player player);

    Player convert(P player);

}
