package ru.zenegix.npcmanager.bukkit;

import ru.zenegix.npcmanager.NpcManager;

import java.util.Random;
import java.util.UUID;

public class BukkitInternal implements NpcManager.Internal {

    private static final Random RANDOM = new Random();

    private static final Class<?> ENTITY_CLASS = Reflection.getClass("{nms}.Entity");

    private static final Reflection.FieldAccessor<Integer> ENTITY_COUNT = Reflection.getField(ENTITY_CLASS, "entityCount", int.class);

    @Override
    public UUID randomUUID() {
        long var1 = RANDOM.nextLong() & -61441L | 16384L;
        long var3 = RANDOM.nextLong() & 4611686018427387903L | -9223372036854775808L;

        return new UUID(var1, var3);
    }

    @Override
    public int getNextEntityId() {
        int count = ENTITY_COUNT.get(null) + 1;
        ENTITY_COUNT.set(null, count);

        return count;
    }

}
