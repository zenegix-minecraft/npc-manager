package ru.zenegix.npcmanager.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import ru.zenegix.npcmanager.data.NpcPosition;
import ru.zenegix.npcmanager.player.PlayerService;
import ru.zenegix.npcmanager.utils.MathUtils;

import java.util.Collection;

public class BukkitPlayerServiceAndMapper implements PlayerService<Player>, PlayerMapper<Player> {

    public static final BukkitPlayerServiceAndMapper INSTANCE = new BukkitPlayerServiceAndMapper();

    private BukkitPlayerServiceAndMapper() {}

    @Override
    public double distance(Player player, NpcPosition position) {
        Location location = player.getLocation();

        return MathUtils.distanceSquared(
                location.getX(), location.getY(), location.getZ(),
                position.getX(), location.getY(), location.getZ()
        );
    }

    @Override
    public Collection<Player> getAll() {
        return (Collection<Player>) Bukkit.getOnlinePlayers();
    }

    @Override
    public Player convert(Player player) {
        return player;
    }

}
